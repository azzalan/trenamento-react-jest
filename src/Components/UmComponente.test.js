import React from 'react'
import TestRenderer from 'react-test-renderer'
import ShallowRenderer from 'react-test-renderer/shallow'
import { UmComponente } from './UmComponente'

it('UmComponente renderiza conteúdo', () => {
    const renderer = new ShallowRenderer()
    renderer.render(
        <UmComponente />,
    )
    const component = renderer.getRenderOutput()
    expect(component.type).toBe('div');
    expect(component.props.children).toEqual('umConteúdo')
})
